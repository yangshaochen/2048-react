var path = require('path');

module.exports = {
  entry: path.resolve(__dirname, 'app/app.js'),
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js'
  },
  module: {
  	loaders: [
  	{
  		test: /\.jsx?$/,
  		exclude: /node_modules/,
  		loaders: ['react-hot', 'babel?presets[]=es2015,presets[]=react']
  		// query: {
  		// 	presets: ['es2015', 'react']
  		// }
  	},
  	{ 
  		test: /\.css$/, 
  		exclude: /node_modules/,
  		loaders: ['react-hot', 'style!css']
  	},
   	{ 
  		test: /\.less$/, 
  		exclude: /node_modules/,
  		//loaders: ['react-hot', 'style!css!less']
  		loader: 'style!css!less'
  	},
  	]
  }
};