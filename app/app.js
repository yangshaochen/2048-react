var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');


// add function to array prototype
Array.prototype.verticalConcat = function(arr) {
	var originalArr = this;
	var originalIdx = 0;
	var arrIdx = 0;
	var newArr = new Array();
	var newIdx = new Array();
	var newLen = originalArr.length + arr.length;
	var offset = newLen/4;

	for (var i=0; i<4; i++) {
		newIdx.push((i+1)*offset-1);
	}
	for (var i=0; i<newLen; i++) {
		if (newIdx.indexOf(i) != -1) {
			newArr[i] = arr[arrIdx];
			arrIdx++;
		}
		else {
			newArr[i] = originalArr[originalIdx];
			originalIdx++;
		}
	}
	return newArr;
};
// React components
var Container = React.createClass({
	setInitial: function(){
		// data array representing 4*4 cell matrix
		// first row is cellValue[0-3], and so on
		var cellValue = new Array(16);
		for (var i=0; i<cellValue.length; i++) {
			cellValue[i] = 0;
		}
		// set initial cells
		var init_1 = Math.floor(Math.random()*16);
		var init_2 = Math.floor(Math.random()*16);
		cellValue[init_1] = 2;
		cellValue[init_2] = 2;

		return cellValue;
	},
	restartGame: function() {
		this.setState({
			cells: this.setInitial(),
			score: 0
		});
		return;
	},
	getInitialState: function() {
		return {
			cells: this.setInitial(),
			score: 0
		};
	},
	getKeyEvent: function(){
		$(document).keydown(function(e){
			var arr = new Array();
			switch(e.keyCode) {
				case 37: // left
				{
					for (var i=0; i<4; i++) {
						arr = arr.concat(this.mergeCell(this.state.cells.slice(i*4, i*4+4)));
					}
					this.afterMove(arr, 'left');
					break;
				}
				case 38: // up
				{
					for (var i=0; i<4; i++) {
						arr = arr.verticalConcat(this.mergeCell([
							this.state.cells[i],
							this.state.cells[i+4*1],
							this.state.cells[i+4*2],
							this.state.cells[i+4*3]]));
					}
					this.afterMove(arr, 'up');
					break;
				}
				case 39: // right
				{
					for (var i=0; i<4; i++) {
						arr = arr.concat(this.mergeCell(
							this.state.cells.slice(i*4, i*4+4).reverse()).reverse());
					}
					this.afterMove(arr, 'right');
					break;
				}
				case 40: // down
				{
					for (var i=0; i<4; i++) {
						arr = arr.verticalConcat(this.mergeCell([
							this.state.cells[i],
							this.state.cells[i+4*1],
							this.state.cells[i+4*2],
							this.state.cells[i+4*3]].reverse()).reverse());
					}
					this.afterMove(arr, 'down');
					break;
				}
			}
		}.bind(this));
	},

	mergeCell: function(arr) {
		// erase 0-value cell
		var newArr = arr.filter(function(ele) {
			return (ele != 0);
		});
		// merge if possible
		for (var i=0; i<newArr.length; i++) {
			if (newArr[i] == newArr[i+1]) {
				newArr[i] = newArr[i] * 2;
				newArr[i+1] = 0;
				break;
			}
		}
		newArr = newArr.filter(function(ele) {
			return (ele != 0);
		});
		for (var i=0; i<4; i++) {
			if (typeof(newArr[i]) == 'undefined') {
				newArr[i] = 0;
			}
		}
		return newArr;
	},
	addNewCell: function(arr) {
		var indexArr = new Array();
		for (var i=0; i<arr.length; i++) {
			if (arr[i] == 0) {
				indexArr.push(i);
			}
		}
		var idx = indexArr[Math.floor(Math.random()*indexArr.length)];
		arr[idx] = 2;
		return arr;
	},
	gameOver: function(arr) {
		var judgeArr = new Array();
		for (var i=0; i<4; i++) {
			judgeArr.push(arr[i*4] - arr[i*4+1]);
			judgeArr.push(arr[i*4+1] - arr[i*4+2]);
			judgeArr.push(arr[i*4+2] - arr[i*4+3]);
		}
		for (var i=0; i<4; i++) {
			judgeArr.push(arr[i] - arr[i+4*1]);
			judgeArr.push(arr[i+4*1] - arr[i+4*2]);
			judgeArr.push(arr[i+4*2] - arr[i+4*3]);
		}
		return judgeArr.every(function (ele) {return ele != 0;});
	},
	afterMove: function(arr, movement) {
		var score = 0;

		if (arr.some(function (ele) {return ele == 0;})) {
			if (this.state.cells.toString() != arr.toString()) {
				arr = this.addNewCell(arr);
				this.setState({cells: arr});
			}
		}
		else if (this.gameOver(arr)) {
			alert('Game Over');
		}

		// calculate score
		for (var i=0; i<this.state.cells.length; i++){
			score += this.state.cells[i];
		}
		this.setState({score: score});
		return;
	},
	changeColor: function() {
		// change cell backgroud color according to its numeric value
		var cellId;
		var num;
		var offset;
		var rgb;

		for (var i=0; i<16; i++) {
			// num = 0,2,4,8,16,32,...
			cellId = 'cell-' + i;
			num = $('#'+cellId).text();
			if (num == 0) {
				offset = 0;
			}
			else {
				offset = Math.log(num)/Math.log(2);
			}
			switch(offset) {
				case 0: {rgb = 'rgb(193,111,28)'; break;}
				case 1: {rgb = 'rgb(251,236,213)'; break;}
				case 2: {rgb = 'rgb(240,183,122)'; break;}
				case 3: {rgb = 'rgb(227,82,24)'; break;}
				case 4: {rgb = 'rgb(149,20,6)'; break;}
				case 5: {rgb = 'rgb(132,41,89)'; break;}
				case 6: {rgb = 'rgb(142,57,239)'; break;}
				case 7: {rgb = 'rgb(64,50,230)'; break;}
				case 8: {rgb = 'rgb(55,120,250)'; break;}
				case 9: {rgb = 'rgb(108,213,250)'; break;}
				case 10: {rgb = 'rgb(32,62,74)'; break;}
				default: {rgb = 'rgb(239,174,44)'; break;}
			}
			$('.gameCell')[i].style.backgroundColor = rgb;
		}
		return;
	},
	componentDidMount: function() {
		this.getKeyEvent();
		this.changeColor();
	},
	componentDidUpdate: function() {
		this.changeColor();
	},
	render: function() {
		return (
			<div className="container">
				<div className="pannel">
					<StartBtn handleClick={this.restartGame} cells={this.state.cells}></StartBtn>
					<Score score={this.state.score}></Score>
				</div>
				<GameBoard data={this.state.cells}></GameBoard>
			</div>
		);
	}
});

var GameBoard = React.createClass({
	render: function() {
		var gameCells = new Array();
		for (var i=0; i<16; i++) {
			gameCells.push(<Cell cellData={this.props.data[i]} key={i} cellId={i}></Cell>);
		}
		return (
			<div className="gameBoard">{gameCells}</div>
		);
	}
});

var Cell = React.createClass({
	render: function() {
		var eleId = 'cell-' + this.props.cellId;
		return (
			<div className="gameCell" id={eleId}>{this.props.cellData}</div>
		);	
	}
});

var Score = React.createClass({
	render: function() {
		return (
			<div className="scoreboard">Score: {this.props.score}</div>
		);
	}
});

var StartBtn = React.createClass({
	render: function() {
		return (
			<button className="newGame" onClick={this.props.handleClick}>New Game</button>
		);
	}
});
ReactDOM.render(
	<Container></Container>,
	$('#content')[0]
);

// css
require('./app.less');